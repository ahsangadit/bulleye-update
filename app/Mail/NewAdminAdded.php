<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAdminAdded extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user;
    public function __construct($user, $password)
    {
        $this->user=$user;
        $this->password=$password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->user);
        // emails.new_admin_added
        return $this->view('maileclipse::templates.newAdminAdded')
                ->from("anthony@straightforwardsuccess.com", "Business BullsEye Admin")
                ->subject("Business BullsEye - You are added as Admin")
                ->with('user',  $this->user)
                ->with('password', $this->password);
    }
}
