<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewClientAdded extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    protected $user;
    protected $package;
    public function __construct($user,$package,$client_password)
    {
        $this->user=$user;
        // dd($this->user);
        $this->package=$package;
        $this->password=$client_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // ->subject("Business BullsEye - You are assigned to a New Package")
        // emails.new_client_added
        return $this->view('maileclipse::templates.welcomeClient')
                ->from("anthony@straightforwardsuccess.com", "Business BullsEye Admin")
                ->subject("Business BullsEye - Welcome New Client")
                ->with('user',  $this->user)
                ->with('package',  $this->package)
                ->with('password',  $this->password);
    }
}
