<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientPasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$password)
    {
        $this->email=$email;
        $this->password=$password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // emails.client_password_reset
        return $this->view('maileclipse::templates.clientPasswordReset')
        ->from("anthony@straightforwardsuccess.com", "Business BullsEye Admin")
        ->subject("Business BullsEye - Your password Reset")
        ->with('email',   $this->email)
        ->with('password',   $this->password);

    }
}
