<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMailNewClient extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    protected $user;
    protected $package;
    public function __construct($user,$package)
    {
        $this->user=$user;
        $this->package=$package;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // emails.admin_new_client_added
        return $this->view('maileclipse::templates.adminMailNewClient')
                ->from("anthony@straightforwardsuccess.com", "Business BullsEye Admin")
                ->subject("Business BullsEye - You are assigned to a New Package")
                ->with('user',  $this->user)
                ->with('package',  $this->package);
    }
}
