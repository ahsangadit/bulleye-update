<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'https://bulleye.hztech.biz/public/coaches/bill_payment_deactivate',
        'https://bulleye.hztech.biz/public/coaches/authorize/bill_payment_deactivate'
    ];
}
