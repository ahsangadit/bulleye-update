<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Mail\NewCoachAdded;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class CoacheController extends Controller {
    
    private $_api_context;

    public function __construct()
    {
        /** setup PayPal api context **/
        $paypal_conf = \Config::get('paypal');
       // dd($paypal_conf);
       $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
       $this->_api_context->setConfig($paypal_conf['settings']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $collection = \App\assign::all();
        $users = \App\User::all();
        $coaches = \App\User::whereIn('status', [1, 2])->orderBy('name', 'asc')->get();
//                $collection->where('role_id', \App\role::coache())->unique("user_id");
//        return $coaches;



        return view('coache.index')->with('collection', $collection)
                        ->with('users', $users)->with("coaches", $coaches);
    }

    /**
     * Display a listing of the active packages.
     *
     * @return \Illuminate\Http\Response
     */
    public function activePackages(Request $request) {
        session(['role' => 'coach']);

        if (\Auth::user()->isAdmin()) {
            $assignments = \App\assignment::where('role_id', \App\role::coache())->get();
//            dd($assignments);
        }
        else {
            $assignments = \App\assignment::where('role_id', \App\role::coache())->where("user_id", \Auth::user()->id)->active()->get();
        }
        // dd($assignments);
        $collection = \App\assignment::all();
        $users = \App\User::all();

        return view('coache.activepack')->with('assignments', $assignments->unique("package_id"))
                        ->with('collection', $collection)->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function registerform(){
        return view('coache.register');
    }

    protected function create(array $data) {
        return User::create([
                    'name'      => $data['name'],
                    'email'     => $data['email'],
                    'password'  => bcrypt($data['password']),
                    'status'    => '2',
                    'paymentId' => $data['paymentId']
        ]);
    }

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    use RegistersUsers;

    public function store(Request $request) {

        try {
            $user = \App\User::where('email', $request->email)->first();
            $error = 0;
      
            if (is_null($user)) {
                $payer   = new Payer();
                $payer->setPaymentMethod('paypal');
                
                $item_1    = new Item();
           //     $item_list = new Item();
           
                $item_1->setName("Register Coach")->setCurrency('USD')->setQuantity('1')->setPrice('10');
          
                $item_list     = new ItemList();
                $item_list->setItems(array($item_1));
            //    dd( $item_list);
                $amount    = new Amount();
                $amount->setCurrency('USD')->setTotal(10);
          
                $transaction = new Transaction();
                $transaction->setAmount($amount)->setItemList($item_list)
                        ->setDescription('Your transaction description');
                
                $redirect_urls  = new RedirectUrls();
                $redirect_urls->setReturnUrl(route('coachorders.status',$request->all() ))/** Specify return URL **/
                    ->setCancelUrl(route('coachorders.status',$request->all()));
               
                $payment        = new Payment();
                $payment->setIntent('Sale')
                        ->setPayer($payer)
                        ->setRedirectUrls($redirect_urls)
                        ->setTransactions(array($transaction));
                    /** dd($payment->create($this->_api_context));exit; **/
                try {
                    $payment->create($this->_api_context);
                    
                } catch (\PayPal\Exception\PPConnectionException $ex) {
                    if (\Config::get('app.debug')) {
                        \Session::put('error', 'Connection timeout');
                        return Redirect::route('addmoney.paywithpaypal');
                    } else {
                        \Session::put('error', 'Some error occur, sorry for inconvenient');
                        return Redirect::route('addmoney.paywithpaypal');
                    }
                }

                foreach ($payment->getLinks() as $link) {
                    if ($link->getRel() == 'approval_url') {
                        $redirect_url = $link->getHref();
                        break;
                    }
                }

                /** add payment ID to session **/
                Session::put('paypal_payment_id', $payment->getId());

                if (isset($redirect_url)) {
                    /** redirect to paypal **/
                    return Redirect::away($redirect_url);
                }

                    \Session::put('error', 'Unknown error occurred');
                    return Redirect::route('addmoney.paywithpaypal');

            } elseif ($user->first()->status != 0) {
                $user->status = 2;
                $user->save();
//                \Mail::to($user->email)->send(new NewCoachAdded($user));
            } else {
                $user = "already exist";
                $error = 1;
            }

            return response()->json([$user, $error]);
        } catch (\Exception $e) {

            abort(500, 'OOps!!Some thing went wrong. Please try again.' . $user);
        }
    }


   


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($coach_id) {
//         $request->coache_id
        $this->authorize('destroyCoach', \App\User::class);
        $user = \App\User::find($coach_id);
        if (\App\assignment::where('user_id', $user->id)->count() > 0) {
            $coachRec = $user->assignments()->coach()->get();

            $umodules = \App\module::where('author_id', '=', $user->id)->delete();
            $upack = \App\package::whereIn('id', $coachRec->pluck('package_id'))->delete();
            $uclients = \App\assignment::whereIn('coache_id', $coachRec->pluck('id'))->delete();

            $udelcoach = \App\assignment::destroy($coachRec->pluck('id'));
        }
        if ($user->isClient()) {
            $user->status = 0;
            $user->save();
        } else {  //if (!$user->isAdmin()) 
            $udel = \App\User::destroy($user->id);
        }
        return back()->with('user', $user)->with('success', $user->name . ' has been deleted successfully.');
    }

    public function updateStatus(Request $request) {
        $assgnment = \App\assignment::where('role_id', \App\role::coache())->where('package_id', $request->package_id)->where('user_id', $request->coach_id)->first();
        echo "status : " . $request->status;
        if ($request->status)
            $assgnment->status = 5;
        else
            $assgnment->status = 0;
        $assgnment->save();
//        return back()->with('success', ' The package has been disabled successfully.');
    }

    public function getOrderStatus(Request $request)
    {
     //dd($request->all());
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error','Payment failed');
            return Redirect::route('addmoney.paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/

       
        if ($result->getState() == 'approved') {
        $password =$request->password;
        event(new Registered($user = $this->create($request->all())));
              \Mail::to($user->email)->send(new NewCoachAdded($user,$password));

            \Session::put('success','Payment success');

            return Redirect::route('login');
            //return Redirect::route('addmoney.paywithpaypal');
        }
        \Session::put('error','Payment failed');
        return Redirect::route('addmoney.paywithpaypal');
    }

}
