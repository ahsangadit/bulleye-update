<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Mail\NewClientAdded;
use App\Mail\AdminMailNewClient;
use App\Mail\ClientPasswordReset;



/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

use PayPal\Api\Agreement;
use PayPal\Api\ShippingAddress;

use PayPal\Api\VerifyWebhookSignature;
use PayPal\Api\WebhookEvent;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use JohnConde\Authnet\AuthnetWebhook;


use DateTime;

class ClientController extends Controller
{

    private $_api_context;

    public function __construct()
    {
        /** setup PayPal api context **/
        $paypal_conf = \Config::get('paypal');
       // dd($paypal_conf);
       $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
       $this->_api_context->setConfig($paypal_conf['settings']);
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $collection = \App\assign::all();

        $users = \App\User::all();
     // $coaches = \App\assignment::coach()->get();
      $coaches = \App\assignment::coach()->get()->unique('user_id');
     // $coaches = \App\User::whereIn('status', [1, 2])->orderBy('name', 'asc')->get();
       // dd($coaches);

        return view('client.index')->with('collection', $collection)
            ->with('users', $users)->with("coaches", $coaches);
    }

    public function registerform(){
        return view('client.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create() {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    use RegistersUsers;

    public function store(Request $request)
    {
        $pack_id = $request->package_id;
        $client_password= $request->password;
        $request->password = bcrypt($request->password);
        $package = \App\package::find($request->package_id);


   

//        $user = \App\User::create([
//            'name'=>$request->name, 
//            'email' => $request->email, 
//            'password' => bcrypt($request->password),
//          
//        ]);
        try {
//            if (\App\User::where('email', '=', $request->email)->count() < 1) {
            event(new Registered($user = $this->create($request->all())));
//            }
            \Mail::to($user->email)->send(new NewClientAdded($user, $package,$client_password));
            \Mail::to("anthony@straightforwardsuccess.com")->cc("ahsan.amin334@gmail.com")->send(new AdminMailNewClient($user, $package));
//        $clientRole = \App\role::client();
            $assign = new \App\assign();
//        $client = $assign->client($user->id, $request->package_id);
//            $role_id = \App\role::client();
//            $pack = $assign->getPackage($request->package_id);
//            $coache = $assign->getCoache($request->package_id);

//echo Auth::id();

//dd();
            $assign =  $assign->client($user->id, $pack_id);
//        $client->role_id = $clientRole;
//        $client->user_id = $user->id;
//        $client->package_id = $request->package_id;
//        $client->save();
            $package_clients = $package->linked_clients;

            return response()->json([
                'client' => $package_clients,
                'totalclients' => $package_clients->count()
            ]);
        } catch (\Exception $e) {
//            echo $e;die;
            abort(500, 'User Already Exist.');
            die;
        }
    }

    protected function create(array $data)
    {        
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => 0
        ]);
    }
    

    protected function client_pass_reset(Request $request)
    {

        $request->validate([
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6'
        ]);

        $user = User::find($request->id);
        $email =  $user->email;
        $password = $request->password;
        $user->password = bcrypt($request->password);
        $user->save();
        \Mail::to($email)->send(new ClientPasswordReset($email, $password));
        return back()->with('success','Client password has been been reset successfully.');

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeExisting(Request $request)
    {

        $pack_id = $request->package_id;
        $emails = $request->emails;
        $password = $request->password;
        // dd($emails);
        // $emails = preg_replace('/\s+/', '', $emails);
        $users = \App\User::whereIn('email', $emails)->get();
        $package = \App\package::find($request->package_id);


        $clients = [];
        if ($users->count() < 1)
            abort(500, 'User Not Exist.');

        foreach ($users as $user) {

            if (\App\assignment::where('user_id', $user->id)->where('package_id', $request->package_id)->count() < 1) {
              $assign = new \App\assign();
                $assign->client($user->id, $request->package_id);
                \Mail::to($user->email)->send(new NewClientAdded($user, $package,$password));
                $clients[] = $user->email;
            } else {
                abort(500, 'User Already Exist.');
            }
        }
        // dd($clients);
        $package_clients = $package->linked_clients;
        return response()->json([
            'clients' => implode(", ", $clients),
            'totalclients' => $package_clients->count()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $client_id)
    {
        $this->authorize('destroy', \App\User::class);
        $user = \App\User::find($client_id);
        if(!empty($user)){
            $user->delete();
        }
//        if (isset($request->package_id)) {
//            $udel = \App\assignment::where('package_id', $request->package_id)->where('user_id', $client_id)->delete();
//
//        } else {
//            $udel = \App\assignment::where('coache_id', $request->coache_id)->where('user_id', $client_id)->delete();
//        }
//  return $udel;      
        return back()->with('user', $user)->with('success', $user->name . ' has been deleted successfully.');
    }

    /**
     * Display a listing of the active packages.
     *
     * @return \Illuminate\Http\Response
     */
    public function activePackages(Request $request)
    {
//return "yesr";
        session(['role' => 'client']);
//        $pack = \App\assign::where('role_id', \App\role::client())->where("user_id",\Auth::user()->id)->pluck("package_id")->all();
//        $packages= \App\package::whereIn("id",$pack)->get();

        if (\Auth::user()->isAdmin())
            $collection = \App\assignment::where('role_id', \App\role::client())->get();
        else
            $collection = \App\assignment::where('role_id', \App\role::client())->where("user_id", \Auth::user()->id)->active()->get();

        return view('client.activepack')->with('assignments', $collection->unique("package_id"))->with('collection', $collection);
    }


    public function clientregister(Request $request) {

        try {
            $user = \App\User::where('email', $request->email)->first();
            $error = 0;
      
            if (is_null($user)) {

                if($request->payment_frequency == "monthly"){
                    // Create a new billing plan
                    $plan = new Plan();
                          $plan->setName('Monthly Client Membership')
                          ->setDescription('Every month you will be charge')
                          ->setType('fixed');
          
                          // Set billing plan definitions
                          $paymentDefinition = new PaymentDefinition();
                          $paymentDefinition->setName('Regular Payments')
                          ->setType('REGULAR')
                          ->setFrequency('Month')
                          ->setFrequencyInterval("1")
                          ->setCycles('12')
                          ->setAmount(new Currency(array('value' => 10, 'currency' => 'USD')));
                       
                          $merchantPreferences = new MerchantPreferences();
          
                          $merchantPreferences->setReturnUrl(route('clientorders.status',$request->all()))
                                  ->setCancelUrl(route('clientorders.status',$request->all()))
                                  ->setAutoBillAmount("yes")
                                  ->setInitialFailAmountAction("CONTINUE")
                                  ->setMaxFailAttempts("0")
                                  ->setSetupFee(new Currency(array('value' => 10, 'currency' => 'USD')));
                  
                          $plan->setPaymentDefinitions(array($paymentDefinition));
                          $plan->setMerchantPreferences($merchantPreferences);
          
                          try {
                              $createdPlan = $plan->create($this->_api_context);         
                            
                              try {
                                $patch = new Patch();
                                $value = new PayPalModel('{"state":"ACTIVE"}');
                                $patch->setOp('replace')
                                  ->setPath('/')
                                  ->setValue($value);
                                $patchRequest = new PatchRequest();
                                $patchRequest->addPatch($patch);
                                $createdPlan->update($patchRequest, $this->_api_context);
                                $plan = Plan::get($createdPlan->getId(), $this->_api_context);
                            
                                // Output plan id
                                echo $plan->getId();
                              } catch (PayPal\Exception\PayPalConnectionException $ex) {
                                echo $ex->getCode();
                                echo $ex->getData();
                                die($ex);
                              } catch (Exception $ex) {
                                die($ex);
                              }
                            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                              echo $ex->getCode();
                              echo $ex->getData();
                              die($ex);
                            } catch (Exception $ex) {
                              die($ex);
                            }
                          // Create new agreement
                          $agreement = new Agreement();
                          $agreement->setName('Monthly Client Membership')
                          ->setDescription('Every month you will be charge')
                          ->setStartDate(gmdate("Y-m-d\TH:i:s\Z", strtotime("+1 month", time())));
                         
                          // Set plan id
                          $plan = new Plan();
                          $plan->setId($createdPlan->getId());
                          $agreement->setPlan($plan);
                        
                          // Add payer type
                          $payer = new Payer();
                          $payer->setPaymentMethod('paypal');
                          $agreement->setPayer($payer);
          
                          $redirect_urls  = new RedirectUrls();
                          $redirect_urls->setReturnUrl(route('clientorders.status',$request->all() ))/** Specify return URL **/
                              ->setCancelUrl(route('clientorders.status',$request->all()));
                        
                          try {
                              // Create agreement
                              $agreement = $agreement->create($this->_api_context);
                            //   DD($agreement->getApprovalLink()); 
                              // Extract approval URL to redirect user
                              $approvalUrl = $agreement->getApprovalLink();
                              
                              return Redirect::away( $approvalUrl);
          
                            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                              echo $ex->getCode();
                              echo $ex->getData();
                              die($ex);
                            } catch (Exception $ex) {
                              die($ex);
                            }
          
                          }
                          else if($request->payment_frequency == "yearly"){
          
                                      // Create a new billing plan
                            $plan = new Plan();
                            $plan->setName('Yearly Client Membership')
                            ->setDescription('Every year you will be charge')
                            ->setType('fixed');
          
                            // Set billing plan definitions
                            $paymentDefinition = new PaymentDefinition();
                            $paymentDefinition->setName('Regular Payments')
                            ->setType('REGULAR')
                            ->setFrequency('Year')
                            ->setFrequencyInterval("1")
                            ->setCycles('1')
                            ->setAmount(new Currency(array('value' => 100, 'currency' => 'USD')));
                     
                            $merchantPreferences = new MerchantPreferences();
          
                            $merchantPreferences->setReturnUrl(route('clientorders.status',$request->all()))
                                    ->setCancelUrl(route('clientorders.status',$request->all()))
                                    ->setAutoBillAmount("yes")
                                    ->setInitialFailAmountAction("CONTINUE")
                                    ->setMaxFailAttempts("0")
                                    ->setSetupFee(new Currency(array('value' => 10, 'currency' => 'USD')));
                          
                            $plan->setPaymentDefinitions(array($paymentDefinition));
                            $plan->setMerchantPreferences($merchantPreferences);
          
                            try {
                                $createdPlan = $plan->create($this->_api_context);         
                               
                                try {
                                  $patch = new Patch();
                                  $value = new PayPalModel('{"state":"ACTIVE"}');
                                  $patch->setOp('replace')
                                    ->setPath('/')
                                    ->setValue($value);
                                  $patchRequest = new PatchRequest();
                                  $patchRequest->addPatch($patch);
                                  $createdPlan->update($patchRequest, $this->_api_context);
                                  $plan = Plan::get($createdPlan->getId(), $this->_api_context);
                              
                                  // Output plan id
                                  echo $plan->getId();
                                } catch (PayPal\Exception\PayPalConnectionException $ex) {
                                  echo $ex->getCode();
                                  echo $ex->getData();
                                  die($ex);
                                } catch (Exception $ex) {
                                  die($ex);
                                }
                              } catch (PayPal\Exception\PayPalConnectionException $ex) {
                                echo $ex->getCode();
                                echo $ex->getData();
                                die($ex);
                              } catch (Exception $ex) {
                                die($ex);
                              }
                            // Create new agreement
                            $agreement = new Agreement();
                            $agreement->setName('Yearly Client Membership')
                            ->setDescription('Every Yearly you will be charge')
                            ->setStartDate(gmdate("Y-m-d\TH:i:s\Z", strtotime("+1 month", time())));
                          
                            // Set plan id
                            $plan = new Plan();
                            $plan->setId($createdPlan->getId());
                            $agreement->setPlan($plan);
                          
                            // Add payer type
                            $payer = new Payer();
                            $payer->setPaymentMethod('paypal');
                            $agreement->setPayer($payer);
          
                            $redirect_urls  = new RedirectUrls();
                            $redirect_urls->setReturnUrl(route('clientorders.status',$request->all() ))/** Specify return URL **/
                                ->setCancelUrl(route('clientorders.status',$request->all()));
                          
                            try {
                                // Create agreement
                                $agreement = $agreement->create($this->_api_context);
                              //   DD($agreement->getApprovalLink()); 
                                // Extract approval URL to redirect user
                                $approvalUrl = $agreement->getApprovalLink();
                                
                                return Redirect::away( $approvalUrl);
          
                              } catch (PayPal\Exception\PayPalConnectionException $ex) {
                                echo $ex->getCode();
                                echo $ex->getData();
                                die($ex);
                              } catch (Exception $ex) {
                                die($ex);
                              }
                            }
                        

            } elseif ($user->first()->status != 0) {
                $user->status = 0;
                $user->save();
//                \Mail::to($user->email)->send(new NewCoachAdded($user));
            } else {
                $user = "already exist";
                $error = 1;
            }

            return response()->json([$user, $error]);
        } catch (\Exception $e) {

            abort(500, 'OOps!!Some thing went wrong. Please try again.' . $user);
        }
    }


    public function getOrderStatus(Request $request)
    {

        $token = $request->token;
        $agreement = new \PayPal\Api\Agreement();
   
        try {
          // Execute agreement
         $result =  $agreement->execute($token, $this->_api_context);
         if(isset($result->id)){
            $package = \App\package::get(); 
            $password =$request->password;
            $user = $this->create($request->all());
            $user_find = User::find($user->id);
            $user_find->agreement_id = $result->id;
            $user_find->subscription_status = '0';
            $user_find->pay_through = 'paypal';
            $user_find->save();   
            event(new Registered($user));
            \Mail::to($user->email)->send(new NewClientAdded($user,$package,$password));
         }
          return redirect()->route('login')->with('success', 'New Client Created and Billed');

        } catch (PayPal\Exception\PayPalConnectionException $ex) {
          return redirect()->route('login')->with('waning', 'You have either cancelled the request or your session has expired');
        }

    }


    public function clientregisterwithauthorize(Request $request) {

      $request->validate([
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
        'password_confirmation' => 'min:6',
        'cardnumber' => 'required|max:255',
        'cvv' => 'required',
        'expiration_month' => 'required',
        'expiration_year' => 'required',
        ]);
   
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        $password_enc = bcrypt($request->password);
        $cardnumber = $request->cardnumber;
        $pay_frequency = $request->payment_frequency;
        $cvv = $request->email;
        $expiration_month = $request->expiration_month;
        $expiration_year = $request->expiration_year;
        $intervalLength = 1;
        $start_date = date('Y-m-d');

        $expiry_date = $expiration_year.'-'.$expiration_month;
      
      /* Create a merchantAuthenticationType object with authentication details retrieved from the constants file */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('8Cr2z44RH2S');
        $merchantAuthentication->setTransactionKey('98K3qzy26Ss9NT2f');
     
        if($pay_frequency == 'monthly'){
          $totalcycles = 12;
          $intervalLength = 1;
          $amount= 10;
     
              // Set the transaction's refId
              $refId = 'ref' . time();
              // Subscription Type Info
              $subscription = new AnetAPI\ARBSubscriptionType();
              $subscription->setName("Coach Monthly Subscription");
        
              $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
              $interval->setLength($intervalLength);
              $interval->setUnit("months");

              $paymentSchedule = new AnetAPI\PaymentScheduleType();
              $paymentSchedule->setInterval($interval);
              $paymentSchedule->setStartDate(new DateTime($start_date));
              $paymentSchedule->setTotalOccurrences($totalcycles);
              //$paymentSchedule->setTrialOccurrences("1");
              $subscription->setPaymentSchedule($paymentSchedule);
              $subscription->setAmount($amount);
              //$subscription->setTrialAmount("0.00");
         
              $creditCard = new AnetAPI\CreditCardType();
              $creditCard->setCardNumber($cardnumber);
              $creditCard->setExpirationDate($expiry_date);
             
              $payment = new AnetAPI\PaymentType();
              $payment->setCreditCard($creditCard);
      
              $subscription->setPayment($payment);

          
              $order = new AnetAPI\OrderType();
              $order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
              $order->setDescription("Client Monthly Subscription");
      
                         
              $subscription->setOrder($order); 
            
              $billTo = new AnetAPI\NameAndAddressType();
              $billTo->setFirstName($name);
              $billTo->setLastName($name);
         
              $subscription->setBillTo($billTo);
           
              $request = new AnetAPI\ARBCreateSubscriptionRequest();
              $request->setmerchantAuthentication($merchantAuthentication);
              $request->setRefId($refId);
              $request->setSubscription($subscription);
            
              $controller = new AnetController\ARBCreateSubscriptionController($request);
        
              $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
        
              if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
              {
                    $package = \App\package::get(); 
                    $sub_id =   $response->getSubscriptionId();
                    $user= new User;
                    $user->name=$name;
                    $user->email=$email;
                    $user->password=$password_enc;
                    $user->status = '2';
                    $user->agreement_id = $sub_id;
                    $user->subscription_status = '1';
                    $user->pay_through = 'authorize.net';
                    $user->save();
                    // dd($user);
               //     $user = $this->create($request->all());
                    // $user_find = User::find($user->id);
                    event(new Registered($user));
                    \Mail::to($user->email)->send(new NewClientAdded($user,$package,$password));
               //    echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
               return redirect()->route('login')->with('success', 'New Client Created and Billed');
               }
              else
              {
                  echo "ERROR :  Invalid response\n";
                  $errorMessages = $response->getMessages()->getMessage();
                  echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
                  return redirect()->route('login')->with('Warning', 'Client Create failed');
              }
          }

        else if($pay_frequency == 'yearly'){
                $totalcycles = 1;
                $intervalLength = 12;
                $amount= 100;
                          /* Create a merchantAuthenticationType object with authentication details
                 retrieved from the constants file */
                  
                 // Set the transaction's refId
                 $refId = 'ref' . time();
                 // Subscription Type Info
                 $subscription = new AnetAPI\ARBSubscriptionType();
                 $subscription->setName("Coach Yearly Subscription");
 
                 $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
                 $interval->setLength($intervalLength);
                 $interval->setUnit("months");
   
                 $paymentSchedule = new AnetAPI\PaymentScheduleType();
                 $paymentSchedule->setInterval($interval);
                 $paymentSchedule->setStartDate(new DateTime($start_date));
                 $paymentSchedule->setTotalOccurrences($totalcycles);
                 //$paymentSchedule->setTrialOccurrences("1");
                 $subscription->setPaymentSchedule($paymentSchedule);
                 $subscription->setAmount($amount);
                 //$subscription->setTrialAmount("0.00");
               
                 $creditCard = new AnetAPI\CreditCardType();
                 $creditCard->setCardNumber($cardnumber);
                 $creditCard->setExpirationDate($expiry_date);
                
                 $payment = new AnetAPI\PaymentType();
                 $payment->setCreditCard($creditCard);
  
                 $subscription->setPayment($payment);
        
             
                 $order = new AnetAPI\OrderType();
                 $order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
                 $order->setDescription("Client Yearly Subscription");
                 
                    
                 $subscription->setOrder($order); 
              
                 $billTo = new AnetAPI\NameAndAddressType();
                 $billTo->setFirstName($name);
                 $billTo->setLastName($name);
         
                 $subscription->setBillTo($billTo);
                 
                 $request = new AnetAPI\ARBCreateSubscriptionRequest();
                 $request->setmerchantAuthentication($merchantAuthentication);
                 $request->setRefId($refId);
                 $request->setSubscription($subscription);
            
                 $controller = new AnetController\ARBCreateSubscriptionController($request);
         
                 $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
            
                 if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
                 {
                  $package = \App\package::get(); 
                   $sub_id =   $response->getSubscriptionId();
                  $user= new User;
                  $user->name=$name;
                  $user->email=$email;
                  $user->password=$password_enc;
                  $user->status = '2';
                  $user->agreement_id = $sub_id;
                  $user->subscription_status = '1';
                  $user->pay_through = 'authorize.net';
                  $user->save();
                 //     $user = $this->create($request->all());
                      // $user_find = User::find($user->id);
                      event(new Registered($user));
                      \Mail::to($user->email)->send(new NewClientAdded($user,$package,$password));
                 //    echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
                 return redirect()->route('login')->with('success', 'New Client Created and Billed');
                  }
                 else
                 {
                  
                    
                  // dd($response);
                     //echo "ERROR :  Invalid response\n";
                     $errorMessages = $response->getMessages()->getMessage();
                    // echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
               
                     return redirect()->route('clientregister')->with('responseerror',$errorMessages[0]->getText());
                     

                 }

             }
          
        }




}
