<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\User;
use auth;
use App\Mail\NewCoachAdded;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\ShippingAddress;

use PayPal\Api\VerifyWebhookSignature;
use PayPal\Api\WebhookEvent;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use JohnConde\Authnet\AuthnetWebhook;


use DateTime;

class CoacheController extends Controller {
    
    private $_api_context;

    public function __construct()
    {
        /** setup PayPal api context **/
        $paypal_conf = \Config::get('paypal');
       // dd($paypal_conf);
       $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
       $this->_api_context->setConfig($paypal_conf['settings']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $collection = \App\assign::all();
        $users = \App\User::all();
        $coaches = \App\User::whereIn('status', [1, 2])->orderBy('name', 'asc')->get();

//                $collection->where('role_id', \App\role::coache())->unique("user_id");
//        return $coaches;



        return view('coache.index')->with('collection', $collection)
                        ->with('users', $users)->with("coaches", $coaches);
    }

    /**
     * Display a listing of the active packages.
     *
     * @return \Illuminate\Http\Response
     */
    public function activePackages(Request $request) {
        session(['role' => 'coach']);

        if (\Auth::user()->isAdmin()) {
            $assignments = \App\assignment::where('role_id', \App\role::coache())->get();
//            dd($assignments);
        }
        else {
            $assignments = \App\assignment::where('role_id', \App\role::coache())->where("user_id", \Auth::user()->id)->active()->get();
        }
        // dd($assignments);
        $collection = \App\assignment::all();
        $users = \App\User::all();
        if(Auth::user()->isCoach() && Auth::user()->subscription_status == '0'){
          return view('coache.nopackage');
        }else{
          return view('coache.activepack')->with('assignments', $assignments->unique("package_id"))
          ->with('collection', $collection)->with('users', $users);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function registerform(){
        return view('coache.register');
    }

    protected function create(array $data) {
        return User::create([
                    'name'      => $data['name'],
                    'email'     => $data['email'],
                    'password'  => bcrypt($data['password']),
                    'status'    => '2',
        ]);
    }

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    use RegistersUsers;

    public function store(Request $request) {

      $request->validate([
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
        'password_confirmation' => 'min:6'
        ]);

        try {
            $user = \App\User::where('email', $request->email)->first();
            $error = 0;
      
            if (is_null($user)) {
          if($request->payment_frequency == "monthly"){
          // Create a new billing plan
          $plan = new Plan();
                $plan->setName('Monthly Coach Membership')
                ->setDescription('Every month you will be charge')
                ->setType('fixed');

                // Set billing plan definitions
                $paymentDefinition = new PaymentDefinition();
                $paymentDefinition->setName('Regular Payments')
                ->setType('REGULAR')
                ->setFrequency('Month')
                ->setFrequencyInterval("1")
                ->setCycles('12')
                ->setAmount(new Currency(array('value' => 10, 'currency' => 'USD')));
             
                $merchantPreferences = new MerchantPreferences();

                $merchantPreferences->setReturnUrl(route('coachorders.status',$request->all()))
                        ->setCancelUrl(route('coachorders.status',$request->all()))
                        ->setAutoBillAmount("yes")
                        ->setInitialFailAmountAction("CONTINUE")
                        ->setMaxFailAttempts("0")
                        ->setSetupFee(new Currency(array('value' => 10, 'currency' => 'USD')));
        
                $plan->setPaymentDefinitions(array($paymentDefinition));
                $plan->setMerchantPreferences($merchantPreferences);

                try {
                    $createdPlan = $plan->create($this->_api_context);         
                  
                    try {
                      $patch = new Patch();
                      $value = new PayPalModel('{"state":"ACTIVE"}');
                      $patch->setOp('replace')
                        ->setPath('/')
                        ->setValue($value);
                      $patchRequest = new PatchRequest();
                      $patchRequest->addPatch($patch);
                      $createdPlan->update($patchRequest, $this->_api_context);
                      $plan = Plan::get($createdPlan->getId(), $this->_api_context);
                  
                      // Output plan id
                      echo $plan->getId();
                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                      echo $ex->getCode();
                      echo $ex->getData();
                      die($ex);
                    } catch (Exception $ex) {
                      die($ex);
                    }
                  } catch (PayPal\Exception\PayPalConnectionException $ex) {
                    echo $ex->getCode();
                    echo $ex->getData();
                    die($ex);
                  } catch (Exception $ex) {
                    die($ex);
                  }
                // Create new agreement
                $agreement = new Agreement();
                $agreement->setName('Monthly Coach Membership')
                ->setDescription('Every month you will be charge')
                ->setStartDate(gmdate("Y-m-d\TH:i:s\Z", strtotime("+1 month", time())));
               
                // Set plan id
                $plan = new Plan();
                $plan->setId($createdPlan->getId());
                $agreement->setPlan($plan);
              
                // Add payer type
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');
                $agreement->setPayer($payer);

                $redirect_urls  = new RedirectUrls();
                $redirect_urls->setReturnUrl(route('coachorders.status',$request->all() ))/** Specify return URL **/
                    ->setCancelUrl(route('coachorders.status',$request->all()));
              
                try {
                    // Create agreement
                    $agreement = $agreement->create($this->_api_context);
                  //   DD($agreement->getApprovalLink()); 
                    // Extract approval URL to redirect user
                    $approvalUrl = $agreement->getApprovalLink();
                    
                    return Redirect::away( $approvalUrl);

                  } catch (PayPal\Exception\PayPalConnectionException $ex) {
                    echo $ex->getCode();
                    echo $ex->getData();
                    die($ex);
                  } catch (Exception $ex) {
                    die($ex);
                  }

                }
                else if($request->payment_frequency == "yearly"){

                            // Create a new billing plan
                  $plan = new Plan();
                  $plan->setName('Yearly Coach Membership')
                  ->setDescription('Every year you will be charge')
                  ->setType('fixed');

                  // Set billing plan definitions
                  $paymentDefinition = new PaymentDefinition();
                  $paymentDefinition->setName('Regular Payments')
                  ->setType('REGULAR')
                  ->setFrequency('Year')
                  ->setFrequencyInterval("1")
                  ->setCycles('1')
                  ->setAmount(new Currency(array('value' => 100, 'currency' => 'USD')));
           
                  $merchantPreferences = new MerchantPreferences();

                  $merchantPreferences->setReturnUrl(route('coachorders.status',$request->all()))
                          ->setCancelUrl(route('coachorders.status',$request->all()))
                          ->setAutoBillAmount("yes")
                          ->setInitialFailAmountAction("CONTINUE")
                          ->setMaxFailAttempts("0")
                          ->setSetupFee(new Currency(array('value' => 10, 'currency' => 'USD')));
                
                  $plan->setPaymentDefinitions(array($paymentDefinition));
                  $plan->setMerchantPreferences($merchantPreferences);

                  try {
                      $createdPlan = $plan->create($this->_api_context);         
                     
                      try {
                        $patch = new Patch();
                        $value = new PayPalModel('{"state":"ACTIVE"}');
                        $patch->setOp('replace')
                          ->setPath('/')
                          ->setValue($value);
                        $patchRequest = new PatchRequest();
                        $patchRequest->addPatch($patch);
                        $createdPlan->update($patchRequest, $this->_api_context);
                        $plan = Plan::get($createdPlan->getId(), $this->_api_context);
                    
                        // Output plan id
                        echo $plan->getId();
                      } catch (PayPal\Exception\PayPalConnectionException $ex) {
                        echo $ex->getCode();
                        echo $ex->getData();
                        die($ex);
                      } catch (Exception $ex) {
                        die($ex);
                      }
                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                      echo $ex->getCode();
                      echo $ex->getData();
                      die($ex);
                    } catch (Exception $ex) {
                      die($ex);
                    }
                  // Create new agreement
                  $agreement = new Agreement();
                  $agreement->setName('Yearly Coach Membership')
                  ->setDescription('Every Yearly you will be charge')
                  ->setStartDate(gmdate("Y-m-d\TH:i:s\Z", strtotime("+1 month", time())));
                
                  // Set plan id
                  $plan = new Plan();
                  $plan->setId($createdPlan->getId());
                  $agreement->setPlan($plan);
                
                  // Add payer type
                  $payer = new Payer();
                  $payer->setPaymentMethod('paypal');
                  $agreement->setPayer($payer);

                  $redirect_urls  = new RedirectUrls();
                  $redirect_urls->setReturnUrl(route('coachorders.status',$request->all() ))/** Specify return URL **/
                      ->setCancelUrl(route('coachorders.status',$request->all()));
                
                  try {
                      // Create agreement
                      $agreement = $agreement->create($this->_api_context);
                    //   DD($agreement->getApprovalLink()); 
                      // Extract approval URL to redirect user
                      $approvalUrl = $agreement->getApprovalLink();
                      
                      return Redirect::away( $approvalUrl);

                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                      echo $ex->getCode();
                      echo $ex->getData();
                      die($ex);
                    } catch (Exception $ex) {
                      die($ex);
                    }

                }

                
            } elseif ($user->first()->status != 0) {
                $user->status = 2;
                $user->save();
//                \Mail::to($user->email)->send(new NewCoachAdded($user));
            } else {
                $user = "already exist";
                $error = 1;
            }

            return response()->json([$user, $error]);
        } catch (\Exception $e) {

            abort(500, 'OOps!!Some thing went wrong. Please try again.' . $user);
        }
    }


   


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($coach_id) {
//         $request->coache_id
        $this->authorize('destroyCoach', \App\User::class);
        $user = \App\User::find($coach_id);
        if (\App\assignment::where('user_id', $user->id)->count() > 0) {
            $coachRec = $user->assignments()->coach()->get();

            $umodules = \App\module::where('author_id', '=', $user->id)->delete();
            $upack = \App\package::whereIn('id', $coachRec->pluck('package_id'))->delete();
            $uclients = \App\assignment::whereIn('coache_id', $coachRec->pluck('id'))->delete();

            $udelcoach = \App\assignment::destroy($coachRec->pluck('id'));
        }
        if ($user->isClient()) {
            $user->status = 0;
            $user->save();
        } else {  //if (!$user->isAdmin()) 
            $udel = \App\User::destroy($user->id);
        }
        return back()->with('user', $user)->with('success', $user->name . ' has been deleted successfully.');
    }

    public function updateStatus(Request $request) {
        $assgnment = \App\assignment::where('role_id', \App\role::coache())->where('package_id', $request->package_id)->where('user_id', $request->coach_id)->first();
        echo "status : " . $request->status;
        if ($request->status)
            $assgnment->status = 5;
        else
            $assgnment->status = 0;
        $assgnment->save();
//        return back()->with('success', ' The package has been disabled successfully.');
    }

    public function getOrderStatus(Request $request)
    {
        
                    $token = $request->token;
                    $agreement = new \PayPal\Api\Agreement();
               
                    try {
                      // Execute agreement
                     $result =  $agreement->execute($token, $this->_api_context);
                     if(isset($result->id)){
                        $password =$request->password;
                        $user = $this->create($request->all());
                        $user_find = User::find($user->id);
                        $user_find->agreement_id = $result->id;
                        $user_find->subscription_status = '1';
                        $user_find->pay_through = 'paypal';
                        $user_find->save();
                        event(new Registered($user));
                        \Mail::to($user->email)->send(new NewCoachAdded($user,$password));
                     }
                      return redirect()->route('login')->with('success', 'New Coache Created and Billed');

                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                      return redirect()->route('login')->with('waning', 'You have either cancelled the request or your session has expired');
                    }

                 } 

                 public function webhook(){

                  $bodyReceived = file_get_contents('php://input');
                   file_put_contents('paypal_webhook.txt', $bodyReceived .PHP_EOL.PHP_EOL. json_encode($_REQUEST) .PHP_EOL.PHP_EOL. json_encode($_SERVER));
                  //die;
                  
                  // Receive HTTP headers that you received from PayPal webhook.
                  $headers = getallheaders(); 
                 file_put_contents('paypal_webhook.txt', json_encode($headers), FILE_APPEND);
                  //die;
          
                  /**
                   * Uppercase all the headers for consistency
                   */
                  $headers = array_change_key_case($headers, CASE_UPPER);
          
                  $signatureVerification = new VerifyWebhookSignature();
                  $signatureVerification->setWebhookId('94H11027B3078790R');
                  $signatureVerification->setAuthAlgo($headers['PAYPAL-AUTH-ALGO']);
                  $signatureVerification->setTransmissionId($headers['PAYPAL-TRANSMISSION-ID']);
                  $signatureVerification->setCertUrl($headers['PAYPAL-CERT-URL']);
                  $signatureVerification->setTransmissionSig($headers['PAYPAL-TRANSMISSION-SIG']);
                  $signatureVerification->setTransmissionTime($headers['PAYPAL-TRANSMISSION-TIME']);
          
                  $webhookEvent = new WebhookEvent();
                  $webhookEvent->fromJson($bodyReceived);
                  $signatureVerification->setWebhookEvent($webhookEvent);
                  $request = clone $signatureVerification;
          
                  try {
                      $output = $signatureVerification->post($this->_api_context);
                  //      file_put_contents('paypal_webhook.txt', $output .PHP_EOL.PHP_EOL. json_encode($_REQUEST) .PHP_EOL.PHP_EOL. json_encode($_SERVER));

                  } catch(\Exception $ex) {
                 //  file_put_contents('paypal_webhook.txt', PHP_EOL . PHP_EOL . 'Error: ' . $ex->getMessage(), FILE_APPEND);
                      print_r($ex->getMessage());

                      exit(1);
                  }
          
                  $verificationStatus = $output->getVerificationStatus();
                  $responseArray = json_decode($request->toJSON(), true);
          
                  $event = $responseArray['webhook_event']['event_type'];
          
                  if ($verificationStatus == 'SUCCESS')
                  {
                      file_put_contents('paypal_webhook.txt', PHP_EOL . PHP_EOL . 'EVENT: '.$event, FILE_APPEND);
                     // die;
                      
                      switch($event)
                      {
                          case 'BILLING.SUBSCRIPTION.CANCELLED':
                                 $agreement_id = $responseArray['webhook_event']['resource']['id'];
                                  file_put_contents('paypal_webhook.txt', PHP_EOL . PHP_EOL . 'Agreement_id: '.$agreement_id, FILE_APPEND);
                                 User::where('agreement_id', $agreement_id)->update(['subscription_status' => '0']);
                          break;
                          case 'BILLING.SUBSCRIPTION.SUSPENDED':
                                 break;
                          case 'BILLING.SUBSCRIPTION.EXPIRED':
                                 break;
                          case 'BILLING_AGREEMENTS.AGREEMENT.CANCELLED':
                          // $user = User::where('payer_id',$responseArray['webhook_event']['resource']['payer']['payer_info']['payer_id'])->first();
                          $this->updateStatus($responseArray['webhook_event']['resource']['payer']['payer_info']['payer_id'], 0);
                          break;
                      }
                  }
                  echo $verificationStatus;
                  exit(0);
              }
             
      public function coachregisterwithauthorize(Request $request) {
        
        $request->validate([
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users',
          'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
          'password_confirmation' => 'min:6',
          'cardnumber' => 'required|max:255',
          'cvv' => 'required',
          'expiration_month' => 'required',
          'expiration_year' => 'required',
          ]);

          $name = $request->name;
          $email = $request->email;
          $password = $request->password;
          $password_enc = bcrypt($request->password);
          $cardnumber = $request->cardnumber;
          $pay_frequency = $request->payment_frequency;
          $cvv = $request->email;
          $expiration_month = $request->expiration_month;
          $expiration_year = $request->expiration_year;
          $intervalLength = 1;
          $start_date = date('Y-m-d');

          $expiry_date = $expiration_year.'-'.$expiration_month;
        
        /* Create a merchantAuthenticationType object with authentication details retrieved from the constants file */
          $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
          $merchantAuthentication->setName('8Cr2z44RH2S');
          $merchantAuthentication->setTransactionKey('98K3qzy26Ss9NT2f');

          if($pay_frequency == 'monthly'){
            $totalcycles = 12;
            $intervalLength = 1;
            $amount= 10;
                 
                // Set the transaction's refId
                $refId = 'ref' . time();
                // Subscription Type Info
                $subscription = new AnetAPI\ARBSubscriptionType();
                $subscription->setName("Coach Monthly Subscription");

                $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
                $interval->setLength($intervalLength);
                $interval->setUnit("months");
        
                $paymentSchedule = new AnetAPI\PaymentScheduleType();
                $paymentSchedule->setInterval($interval);
                $paymentSchedule->setStartDate(new DateTime($start_date));
                $paymentSchedule->setTotalOccurrences($totalcycles);
                //$paymentSchedule->setTrialOccurrences("1");
                $subscription->setPaymentSchedule($paymentSchedule);
                $subscription->setAmount($amount);
                //$subscription->setTrialAmount("0.00");
               
                $creditCard = new AnetAPI\CreditCardType();
                $creditCard->setCardNumber($cardnumber);
                $creditCard->setExpirationDate($expiry_date);
               
                $payment = new AnetAPI\PaymentType();
                $payment->setCreditCard($creditCard);
     
                $subscription->setPayment($payment);

            
                $order = new AnetAPI\OrderType();
                $order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
                $order->setDescription("Coach Monthly Subscription");
                
                           
                $subscription->setOrder($order); 
              
                $billTo = new AnetAPI\NameAndAddressType();
                $billTo->setFirstName($name);
                $billTo->setLastName($name);
              
                $subscription->setBillTo($billTo);
               
                $request = new AnetAPI\ARBCreateSubscriptionRequest();
                $request->setmerchantAuthentication($merchantAuthentication);
                $request->setRefId($refId);
                $request->setSubscription($subscription);
             
                $controller = new AnetController\ARBCreateSubscriptionController($request);
              
                $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
               // dd($response);
                if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
                {
                      $sub_id =   $response->getSubscriptionId();
                      $user= new User;
                      $user->name=$name;
                      $user->email=$email;
                      $user->password=$password_enc;
                      $user->status = '2';
                      $user->agreement_id = $sub_id;
                      $user->subscription_status = '1';
                      $user->pay_through = 'authorize.net';
                      $user->save();
                 //     $user = $this->create($request->all());
                      // $user_find = User::find($user->id);
                      event(new Registered($user));
                      \Mail::to($user->email)->send(new NewCoachAdded($user,$password));
                 //    echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
                 return redirect()->route('login')->with('success', 'New Coache Created and Billed');
                 }
                else
                {
                    echo "ERROR :  Invalid response\n";
                    $errorMessages = $response->getMessages()->getMessage();
                    echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
                    return redirect()->route('login')->with('Warning', 'Coach Create failed');
                }
            }
  
          else if($pay_frequency == 'yearly'){
                  $totalcycles = 1;
                  $intervalLength = 12;
                  $amount= 100;
                            /* Create a merchantAuthenticationType object with authentication details
                   retrieved from the constants file */
                    
                   // Set the transaction's refId
                   $refId = 'ref' . time();
                   // Subscription Type Info
                   $subscription = new AnetAPI\ARBSubscriptionType();
                   $subscription->setName("Coach Yearly Subscription");
   
                   $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
                   $interval->setLength($intervalLength);
                   $interval->setUnit("months");
     
                   $paymentSchedule = new AnetAPI\PaymentScheduleType();
                   $paymentSchedule->setInterval($interval);
                   $paymentSchedule->setStartDate(new DateTime($start_date));
                   $paymentSchedule->setTotalOccurrences($totalcycles);
                   //$paymentSchedule->setTrialOccurrences("1");
                   $subscription->setPaymentSchedule($paymentSchedule);
                   $subscription->setAmount($amount);
                   //$subscription->setTrialAmount("0.00");
                 
                   $creditCard = new AnetAPI\CreditCardType();
                   $creditCard->setCardNumber($cardnumber);
                   $creditCard->setExpirationDate($expiry_date);
                  
                   $payment = new AnetAPI\PaymentType();
                   $payment->setCreditCard($creditCard);
    
                   $subscription->setPayment($payment);
          
               
                   $order = new AnetAPI\OrderType();
                   $order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
                   $order->setDescription("Coach Yearly Subscription");
                   
                      
                   $subscription->setOrder($order); 
                
                   $billTo = new AnetAPI\NameAndAddressType();
                   $billTo->setFirstName($name);
                   $billTo->setLastName($name);
           
                   $subscription->setBillTo($billTo);
                   
                   $request = new AnetAPI\ARBCreateSubscriptionRequest();
                   $request->setmerchantAuthentication($merchantAuthentication);
                   $request->setRefId($refId);
                   $request->setSubscription($subscription);
              
                   $controller = new AnetController\ARBCreateSubscriptionController($request);
           
                   $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
              
                   if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
                   {
                     $sub_id =   $response->getSubscriptionId();
                    $user= new User;
                    $user->name=$name;
                    $user->email=$email;
                    $user->password=$password_enc;
                    $user->status = '2';
                    $user->agreement_id = $sub_id;
                    $user->subscription_status = '1';
                    $user->pay_through = 'authorize.net';
                    $user->save();
                   //     $user = $this->create($request->all());
                        // $user_find = User::find($user->id);
                        event(new Registered($user));
                        \Mail::to($user->email)->send(new NewCoachAdded($user,$password));
                   //    echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
                   return redirect()->route('login')->with('success', 'New Coache Created and Billed');
                    }
                   else
                   {
                    
                      
                    // dd($response);
                       //echo "ERROR :  Invalid response\n";
                       $errorMessages = $response->getMessages()->getMessage();
                      // echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
                 
                       return redirect()->route('coachregister')->with('responseerror',$errorMessages[0]->getText());
                       

                   }

               }
            
          }

          
          public function authorizenetwebhook(Request $request){
              
            $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
            $merchantAuthentication->setName('8Cr2z44RH2S');
            $merchantAuthentication->setTransactionKey('98K3qzy26Ss9NT2f');
    
            $headers = getallheaders();
            $payload = file_get_contents("php://input");
            $payloadEncoded = json_encode($payload);
            $payloadDecoded = json_decode($payload);
            $webhook = new AuthnetWebhook('2013194E0695CA41519628DC54330EB625ED531EA76555AF0188097D46D349072D387640DEA1D1AE1B6ADE154CB4834A499D16FB50F727E135848FF3CF96E427', $payloadEncoded, $headers);
             file_put_contents('authorize_webhook.txt', $webhook .PHP_EOL.PHP_EOL. json_encode($_REQUEST) .PHP_EOL.PHP_EOL. json_encode($_SERVER));

    
            // $refId = 'ref' . time();
            // $request = new AnetAPI\ARBCancelSubscriptionRequest();
            // $request->setMerchantAuthentication($merchantAuthentication);
            // $request->setRefId($refId);
            // $request->setSubscriptionId('6386240');
            // $sub_id= $request->setSubscriptionId('6386240');
               $controller = new AnetController\ARBCancelSubscriptionController($request);

            $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
                  dd($response);
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
            {
                User::where('agreement_id', $sub_id)->update(['subscription_status' => '0']);
             }
            else
            {
                echo "ERROR :  Invalid response\n";
                $errorMessages = $response->getMessages()->getMessage();
                echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
                
            }
      
        
         }


        }

      