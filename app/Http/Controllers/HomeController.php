<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
//        return "i m view";
        if(\Auth::user()->isClient()){   
             session(['role' => 'client']);
            return redirect('/clients/active_packages');
        } else if(\Auth::user()->isCoach()){
            session(['role' => 'coach']);
            return redirect('/coaches/active_packages');
        }
        else{
           session(['role' => 'admin']);
            return redirect('/coaches/active_packages');
            //return view('home');
        }
            
    }



    public function displayusers(){
        $users = User::where('status','1')->get();
        return view('users.index',['users'=>$users]);
    }
    
    public function editusers($id){
        $users = User::find($id);
        return view('users.edit',['users'=>$users]);
    }

    public function updateusers(Request $request ,$id){


        $capabilities = $request->capabilities;   
      
        $capabilities_array=array();
       foreach ($capabilities as $key => $value){;
          $capabilities_array[$value] = 'true';
       }

        $users = User::find($id);
        $users->name      = $request->name;
        $users->email     = $request->email;
        $users->password  = bcrypt($request->password);
        $users->status       = 1;
        $users->capabilities = serialize($capabilities_array);
        $users->save();

        return redirect()->route('displayusers');
    }

    public function deleteusers($id){
        $users = User::find($id);
        $users->delete();
        return redirect()->route('displayusers');
    }

}
