<?php

namespace App\Http\Controllers;

use App\module;
use App\module_category;
use Illuminate\Http\Request;

class ModuleController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $modules = module::where('is_live', false)->author()->get();
        $live_modules = module::where('is_live', true)->author()->get();
        
       
        return view('module.index')->with('modules', $modules)
                        ->with('live_modules', $live_modules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id) {
        $module_category = module_category::get();
//        $module = new module();
//        $module->title = "New Module Title";
//        $module->description 
          $cat_id = $id;
        return view('module.create')->with('state','add')->with('module_category',$module_category)->with('cat_id',$cat_id);
    }

    public function create_without_id() {
        $module_category = module_category::get();
//        $module = new module();
//        $module->title = "New Module Title";
//        $module->description 
        return view('module.create')->with('state','add')->with('module_category',$module_category);
    }


    /**
     * Available modules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function getLive() {
        $module = module::all()->where('is_live', true);
        return response()->json($module);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      
      //  $module = module::create($request->all());
      $module = new module();
     // dd($request->all());
      $module->title = $request->title;
      $module->description = $request->description;
      $module->module_category_id = $request->module_category;
      $module->content = $request->content;
      $module->save();

        return response()->json(['module'=>$module, 'url'=>url('modules/'.$module->id)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\module  $module
     * @return \Illuminate\Http\Response
     */
    public function show($module_id) {
        $module = module::find($module_id);
        $assignment = \App\assignment::where('module_id', $module_id)->first();
        $module_category = module_category::get();
//        $assignment = \App\assignment::where('user_id',Auth::user()->id)->where('package_id',$module->package())
        return view('module.preview')->with('module', $module)->with('assignment', $assignment)->with('module_category',$module_category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit($module_id) {

        $module = module::find($module_id);
        $module_category = module_category::get();
//        return response()->json($module);
//        return var_dump($module);
        return  view('module.create')->with('state','update')
                ->with('module',$module)->with('module_category',$module_category);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $module_id) {
        $module = module::find($module_id);
        $module->title = $request->title;
        $module->description = $request->description;
        $module->content = $request->content;
        $module->module_category_id = $request->module_category;
        $module->save();
        return response()->json($module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy($module_id) {
        $module = module::destroy($module_id);
        return response()->json($module);
    }

    /**
     * update to live module.
     *
     * @param  \App\module  $module
     * @return \Illuminate\Http\Response
     */
    public function makeLive($module_id) {
        $module = module::find($module_id);
     
        $module->is_live = true;
        $module->save();
        dd($module);
        return response()->json($module);
    }

    /**
     * copy a module.
     *
     * @param  \App\module  $module
     * @return \Illuminate\Http\Response
     */
    public function makeCopy($module_id) {
        $module = module::find($module_id);
        $copy_of_module = $module->replicate();
        $copy_of_module->title = "[COPY OF]" . $copy_of_module->title;
        $copy_of_module->is_live = 0;
        $copy_of_module->save();
        $copy_questions = [];
        foreach ($module->questions()->get() as $quesCopy) {
            $copy_questions[] = $quesCopy->replicate();
        }
        $copy_of_module->questions()->saveMany($copy_questions);

        $documents_copy = [];
        foreach ($module->documents()->get() as $documentCopy) {
            if(file_exists(public_path('documents/' .  $documentCopy->filename))) {        
                $orignalName =explode('.', $documentCopy->filename);
                $fileName = $orignalName[0]."_" . $copy_of_module->id . "." . $orignalName[1];
                copy(public_path('documents') . '/' . $documentCopy->filename, public_path('documents') . '/' . $fileName);            
                $newCopy=$documentCopy->replicate();
                $newCopy->filename = $fileName;
                $documents_copy[] = $newCopy;
            }            
        }

        $copy_of_module->documents()->saveMany($documents_copy);
        return back()->with('success', 'Module copy successfully.');
    }

    
      // Module Category Functions

    public function view_category() {
       
        if (\Auth::user()->isAdmin()){
            $module_category = module_category::get();
            return view('module.module_cat_view',['category' => $module_category]);
        }

   }

    public function add_category() {
        if (\Auth::user()->isAdmin()){
            return view('module.module_cat_add');
        }

    }

    public function store_category(Request $request) {
        if (\Auth::user()->isAdmin()){
            $title= $request->title;
            $module_category = module_category::create(['cat_name' => $title]);
            return  redirect()->route('view_module_category');
        }

    }
   
    public function edit_category($id) {
        if (\Auth::user()->isAdmin()){
            $module_category = module_category::find($id);
            return view('module.module_cat_add',['module_category'=> $module_category]);
        }

    }

    public function destroy_category($id) {
        if (\Auth::user()->isAdmin()){
            $module_category = module_category::find($id);
            $module_category->delete();
            return  redirect()->route('view_module_category');
        }

    }

    public function update_category(Request $request, $id) {
        if (\Auth::user()->isAdmin()){
            $title= $request->title;
            $module_category = module_category::find($id);
            $module_category->cat_name = $title ;
            $module_category->save();
            return redirect()->route('view_module_category');
        }

    }

    public function module_category(){
        if (\Auth::user()->isAdmin()){
            $count= [];
            $module_category = module_category::get();
            foreach($module_category as $category){
                $count[] = module_category::find($category->id)->module;
            }
            return view('module.module_category',['module_category'=>$module_category,'count'=>$count]); 
        }
      }


      public function get_modules_by_category($id){
        if (\Auth::user()->isAdmin()){
            $modules = module::where('is_live', false)->author()->get();
           // $live_modules = module::where('is_live', true)->author()->get();
            // $module_category = module::where('module_category_id',$id)->get();
        //   $module_category = module::where([['module_category_id', '=', $id],['is_live' ,false]])->get();
            $live_modules = module::where([['module_category_id', '=', $id],['is_live' ,true]])->get();
            $cat_id= $id;
            return view('module.index_new',['modules'=>$modules,'live_modules'=> $live_modules,'cat_id'=>$cat_id]); 
        }
      }

        // Module Category Functions
  
    

}
