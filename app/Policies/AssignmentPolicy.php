<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\assignment;

class AssignmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function sendcoachAlert(User $user , assignment $assignment)
    {    
        // $user_capabilities = array();

        $user= \Auth::user();
     
        if($user->capabilities != ""){
            
            $user_capabilities = unserialize($user->capabilities);
        $true = array_key_exists("sendcoachAlert",$user_capabilities);
        
       if($assignment->package()->first()->status == 0 && ($true))
            $clientReply=true;
        else
            $clientReply=false;

        if($clientReply && ($assignment->coach->id == \Auth::user()->id || \Auth::user()->status == 1) && ($true) ) 
              return true;
    }else{
      
        if($assignment->package()->first()->status == 0)
        $clientReply=true;
         else
             $clientReply=false;
 
         if($clientReply && ($assignment->coach->id == \Auth::user()->id || \Auth::user()->status == 1)  ) 
                return true;
    }
}
    
    public function sendclientAlert(User $user , assignment $assignment)
    {
        // $user_capabilities = array();

        $user= \Auth::user();

        if($user->capabilities != ""){
            $user_capabilities = unserialize($user->capabilities);
        $true = array_key_exists("sendclientAlert",$user_capabilities);
        
       if($assignment->package()->first()->status == 0 && ($true))
            $clientReply=true;
        else
            $clientReply=false;

        if($clientReply && ($assignment->coach->id == \Auth::user()->id || \Auth::user()->status == 1) && ($true) ) 
              return true;
    }else{
        
        if($assignment->package()->first()->status == 0)
         $clientReply=true;
         else
              $clientReply=false;
 
         if($clientReply   ) 
               return true;
    }
}
}
