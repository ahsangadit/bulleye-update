<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
     public function destroy(User $user, User $client)
    {
        return (\Auth::user()->isAdmin());
    }
    
    public function edit(User $user, User $client)
    {
        $user= Auth::user();
        $user_capabilities= unserialize($user->capabilities);
   
        if(array_key_exists('seeclients', $user_capabilities)){
            $edit = $user_capabilities['seeclients'];
        }else{
            $edit = 'false';
        }
        return (Auth::user()->isAdmin() && ($edit == "true") );
    }
}
