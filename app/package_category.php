<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class package_category extends Model
{
    protected $table = 'package_category';
    protected $fillable = ['cat_name'];
    public $timestamps = false;

    public function package() {
        return $this->hasMany('App\package');
    }
}
