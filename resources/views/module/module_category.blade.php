

@extends('layouts.app')

@section('content')

@isset($module_category)
@php
$num=0;    
@endphp
    <div class="row">
        <div class="col-md-12">
            @foreach($module_category as $category)
        {{-- <div class="col-md-3">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-default btn-primary" value="by_client">{{$category->cat_name}}</button>
            </div>
             </div> --}}
             <div class="col-lg-3 col-xs-6" style="text-align: center;padding-bottom:30px;">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <a href="{{route('get_modules_by_category',$category->id)}}" class="small-box-footer"><img src="{{ asset('images/folder-icon.png') }}" style="width:150px" /></a>
                  <div class="inner">
                    <h3 style="font-size:18px;margin-left:0px;margin-top:0px!important;margin-bottom:0px!important;">{{$category->cat_name}} ({{count($count[$num])}})</h3>
                  </div>
                  <div class="icon">
                    <i class="ion ion-bag"></i>
                  </div>
                  {{-- <a href="{{route('get_modules_by_category',$category->id)}}" class="small-box-footer">View Category <i class="fa fa-arrow-circle-right"></i></a> --}}
                </div>
              </div>
              @php
               $num++;    
              @endphp
             @endforeach
        </div>
    </div>


@endisset 
        @include('modals.add_client')
        @include('modals.linked_clients')
        
    </div>






</div>




@endsection

@section('heading')
Module Category  <a href="{{url('modules/add')}}"id="btn_add" name="btn_add" class="btn-general-org-dark pull-right">New Module</a>
@endsection

@section('title')
Packages
@endsection

@section('script')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('js/plugins/multiselect/dist/css/bootstrap-multiselect.css')}}"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="{{asset('js/package.js')}}"></script>
<script src="{{asset('js/plugins/multiselect/dist/js/bootstrap-multiselect.js')}}"></script>
@endsection


<style>

select#package-assign-multidropdown + div .dropdown-menu {
    position: inherit;
    width: 100%;
    display: block;
}


select#package-assign-multidropdown + div .dropdown-menu>li>a{
    display: table;
    width: 100%;
}

.selected_email ul {
    min-height: 230px;
    box-shadow: 0 0 1px 1px rgba(0,0,0,.5);
    width: 300px;
    overflow: auto;
    padding: 10px;
    list-style: none;
    font-size: 15px;
}

.selected_email h2 {
    margin-top: 0;
}
</style>