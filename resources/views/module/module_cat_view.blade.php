

@extends('layouts.app')

@section('content')

<div class="">
    <div class="row">
        <div class="col-md-12 ">
            <a href="{{route('add_module_category')}}"id="btn_add" name="btn_add" class="btn-general-org-dark pull-right">New Category</a>

@php
$num=1;
@endphp

            <!--Clients Order by module-->
            <div class="panel-body"> 

        @isset($category)
                <table class="table">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Title</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody id="package" name="package-list">
                    
                        @foreach($category as $category)  
                        <tr>
                        <td> {{$num++}} </td>
                        <td>{{$category->cat_name}}</td>
                            <td><a href="{{route('edit_module_category',$category->id)}}" class="btn btn-secondary btn-detail edit_package" title="Edit"><i class="fa fa-edit">Edit</i></a>
                                <form enctype='multipart/form-data' class="form-inline" style="display:inline" role="form" method="POST"  id="deleteForm_{{$category->id}}" action="{{ route('destroy_module_category',$category->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}   
                                <button type="button" class="btn btn-danger btn-delete delete-module-category" id="delete_module_category_{{$category->id}}" title="Delete">
                                    <i class="fa fa-remove"> Delete</i></button></td>
                                </form>
                        </tr>             
                      @endforeach
                   
                    </tbody>
                </table>
            @endisset 
            </div>

        </div>
       
        @include('modals.add_client')
        @include('modals.linked_clients')
        
    </div>






</div>




@endsection

@section('heading')
Module Category <!--<small>management</small>-->
@endsection

@section('title')
module
@endsection

@section('script')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('js/plugins/multiselect/dist/css/bootstrap-multiselect.css')}}"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
<script src="{{asset('js/package.js')}}"></script>
<script src="{{asset('js/plugins/multiselect/dist/js/bootstrap-multiselect.js')}}"></script>
@endsection


<style>

select#package-assign-multidropdown + div .dropdown-menu {
    position: inherit;
    width: 100%;
    display: block;
}


select#package-assign-multidropdown + div .dropdown-menu>li>a{
    display: table;
    width: 100%;
}

.selected_email ul {
    min-height: 230px;
    box-shadow: 0 0 1px 1px rgba(0,0,0,.5);
    width: 300px;
    overflow: auto;
    padding: 10px;
    list-style: none;
    font-size: 15px;
}

.selected_email h2 {
    margin-top: 0;
}
</style>