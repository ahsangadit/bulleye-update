<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Business BullsEye</title>

      <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link href="{{ asset('css/danish-css.css') }}" rel="stylesheet">
        <!--Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       
    </head>
    <body>
    <section  class="head">
        
    <div class="row">
            <div class="col-md-12">
                      @if (Route::has('login'))
            <div class="top-right ">
                @if (Auth::check())
                    <a class="orange-btn" href="{{ url('/home') }}">Home</a>
                @else
                    <a class="orange-btn" href="{{ url('/login') }}"><i class="fa fa-user-o" aria-hidden="true"></i> Login Here</a>
                    <!--<a href="{{ url('/register') }}">Register</a>-->
                @endif
            </div>
        @endif
            </div>
    </div>
      
      
    </section>
    <section class="LoginSection">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <div class="ctr">

                    <img src="{{ asset('images/welcome_logo.png') }}" class="mid-logo">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="ctr MrTp">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis sagittis augue. In ut accumsan nisl. Quisque velit dolor, ullamcorper sed ex id, sollicitudin maximus tortor. In non luctus ipsum, et posuere dui.</p>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        </div> 
    </section>
    <footer class="custom-footer">
    <div class="container">
        <div class="row top-line2">
            <div class="col-md-12">
                <div class="ctr">
                    <p>Copyright ©  2020 Business Bullseye. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>  
</footer>
</body>
</html>
