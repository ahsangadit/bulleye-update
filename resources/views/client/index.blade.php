

@extends('layouts.app')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-12 ">
            <!--<button id="btn_add_package" name="btn_add_package" class="btn btn-secondary pull-right" >New Package</button>-->
            @if ($message = Session::get('success'))
            <div class="success-notification" message="{{ $message }}">
            </div>

            @endif

            <!--Clients Order by Packages-->
            <div class="panel-body"> 


                <table class="table">
                    <thead>
                        <tr>
                            <!--<th style="width: 14px;"></th>-->
                            <th>Name </th>
                            <th> </th>
                        <th> </th>
                        </tr>
                    </thead>
                    <tbody id="clients-list" name="clients-list">
                        <?php // dd($coaches); ?>
                        @foreach ($coaches as $coache)
                
                        <tr id="coache_{{$coache->id}}" class="coaches_list">
                            <!--<td>+</td>-->
                            <td><strong>[{{ ($coache->getUserStatus($users)==2)?'Coach':'Admin'}}]</strong> {{$coache->getEmail($users)}}</td>
                            <td><button class="viewclients green-btn" value="{{$coache->id}}" title="Show Clients"><i class="fa fa-caret-square-o-down" ></i> Show Clients</button></td>
                        </tr>
            
                        @foreach ($coache->getClients($users,$collection) as $client)
                        @if($loop->index == 0)
                        <tr id="actions" style="display:none">
                            <th>Email</th>
                            <th>Reset Password</th>
                            <th>Action</th>
                      
                        </tr>
                        <tr id="coach_{{$coache->id}}" class="coach_clients">
                            <td colspan="3">                 
                                <table class="table">
                                    <tbody id="clients-list" name="clients-list">                            
                                        @endif
                                        <tr class="coach_client">
                                  
                                            <!--<td>-</td>-->
                                            <td>{{$client->email}}</td> 
                                          
                                            <td> <button data-id="{{$client->id}}" type="button" class="btn-sm reset-pass blue-btn" data-toggle="modal" >Reset Password</button></td> 
                                           
                                                <td>
                                                <form enctype='multipart/form-data' class="form-horizontal" role="form" method="POST"  id="deleteForm_{{$client->id}}" action="{{ url("clients/".$client->id) }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="button" class="btn btn-danger btn-delete delete-client " value="{{$client->id}}" id="delete_client_{{$client->id}}"  title="Delete">
                                                        <i class="fa fa-remove" ></i></button>
                                                    <input type="hidden" name="coache_id" value="{{$coache->id}}" />

                                                </form>

                                            </td>
                                        </tr>

                                        @if($loop->count+1 == $loop->last)
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        @endif            

    

                        @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!--       incluides here-->
    </div>



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Reset Password</h4>
            </div>
            <div class="modal-body">
                <form enctype='multipart/form-data'id="reset_password" class="form-horizontal" role="form" method="POST" action="{{route('clientpassreset')}}">
                    {{ csrf_field() }}
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Password : </label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Confirm Password : </label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="confirm_password" required>
                        <input type="hidden" class="id-field" name="id" required>
                        <br>
                        <button type="submit" class="btn btn-info pull-right">save</button>
                        <br>
                    </div> 
                </div>
                   {{-- <div style="display:none">      </div> --}}
                  <p>
                   <span id="validation-errors" class="invalid-feedback" role="alert" style="color: red;">
                    <strong></strong>
                </span>
                <span id="success" role="alert" style="color: green;">
                    <strong></strong>
                </span>
                 </p>
             
            </form>
            </div>
            <div class="modal-footer">
    
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>  

</div>

@endsection

@section('heading')
Clients <!--<small>management</small>-->
@endsection

@section('title')
Clients
@endsection

@section('script')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--<script src="{{asset('js/client.js')}}"></script>-->
<script>
    $(document).ready(function () {

        $(document).on('click', '.reset-pass', function (e) {
            // $('#myModal').modal('toggle');
            $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#validation-errors strong').html('');
        $("#success strong").html('');
            id= $(this).data('id')
            $('.id-field').val(id);


            $("#reset_password").submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url:'{{url("/clients/reset")}}',
                    type:'POST',
                    data: formData,
                    success: function(data){
                        $("#success").show();
                        $('#validation-errors strong').html('');
                        $("#success strong").html('');
                        $('#success strong').html('Client Password Change Succesfully');
                        setTimeout(function() { $('#myModal').modal('hide'); }, 3000);            
                    },
                    error: function(data){
                 
                        $("#validation-errors").show();
                        $('#validation-errors strong').html('');
                    $.each(data.responseJSON.errors, function(key,value) {
                        $('#validation-errors strong').html(value+'<br>');
                    }); 
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            });

   
        });

        console.log("ready!");
        $('[id^=coach_]').hide();
    });
    $(document).on('click', '.viewclients', function (e) {
        var coach_id = $(this).val();
        $('[id^=coach_' + coach_id + ']').toggle();
        $('[id^=actions]').toggle();
    });
    $(document).ready(function () {
        if ($('.success-notification').length) {
            $.notify($('.success-notification').attr("message"));
        }

    });
    $('[id^=delete_client_]').click(function () {
        delbtn = $(this);
        bootbox.confirm({
            title: "Delete Client?",
            message: "Are you sure to delete this client?  it will be deleted from all your subscribed packages",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    console.log(result);
                    console.log(delbtn.val());

                    delbtn.parents('form').submit();

                }
            }
        });
//    return result; //you can just return c because it will be true or false
    });
</script>
@endsection
