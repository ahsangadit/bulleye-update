@extends('layouts.app')

@section('heading')
&nbsp;<!-- col-md-11 Admin User - <small>Add</small>-->
@endsection

@section('content')
<section class="LoginSection">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                   <!--  <div class="panel-heading">Register As Client</div> -->
                    <div class="panel-body">
                        @if(Session::has('responseerror'))
                      
                            <div class="row"> 
                                <div class="col-md-12"> 
                                    <div class="custom-alerts alert alert-danger"><em> {!! session('responseerror') !!}</em> 
                                   </div> 
                               </div> 
                           </div> 
                       
                       @endif 
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                              <!--   <label for="name" class="col-md-4 control-label">Name</label> -->
                            <div class="row">
                                <h1>Register As Admin</h1>
                                     <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                               <!--  <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                                <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                         </div>
                                </div>
                            </div>
                               
                            <div class="row">
                                 <div class="col-md-6">
                                     <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                     </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="top-line MrTp p-1 ">
                                   <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group ctr">
                                            <label> Capabilities </label>
                                          </div>
                                    </div>
                                   </div>
                                   <div class="row">
                                    <div class="col-md-3"></div>
                                     <div class="col-md-3 mobile-ctr">
                                         <input type="checkbox" class="add" name="capabilities[]" value="sendcoachAlert" >
                                         <label for="monthly">Send Coach Alert </label><br>
                                         <input type="checkbox" class="add" name="capabilities[]" value="sendclientAlert" >
                                             <label for="monthly">Send Client Alert  </label><br>
                                             <input type="checkbox" class="add" name="capabilities[]" value="assignCoach" >
                                             <label for="monthly">Assign Coach </label><br>
                                             <input type="checkbox" class="add" name="capabilities[]" value="edit" >
                                         <label for="monthly">Edit Package </label>
                                     </div>
                                     <div class="col-md-3 mobile-ctr">
                                         <input type="checkbox" class="add" name="capabilities[]" value="editmodule" >
                                         <label for="monthly">Edit Module  </label><br>
                                         <input type="checkbox" class="add" name="capabilities[]" value="seeclients" >
                                         <label for="monthly">See Clients</label><br>
                                         <input type="checkbox" class="add" name="capabilities[]" value="destroyCoach" >
                                         <label for="monthly">Destroy Coach </label><br>
                                         <input type="checkbox" class="add" name="capabilities[]" value="emailtemplateedit" >
                                         <label for="monthly">Email template </label>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                                    </div>

                                   </div>

                           <div class="row p-1 ">
                                <div class="col-md-2"></div>
                                 <div class="col-md-12 ctr">
                                     <div class="form-group">
                                        <button type="submit" class="btn btn-outline">
                                            Register
                                        </button>
                                    </div>
                                </div>
                                 <div class="col-md-2"></div>
                                
                           </div>
                        </form>
                    </div>

              
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
