<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Business BullsEye ') }} - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/danish-css.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ahsan-css.css') }}" rel="stylesheet">
    <!--Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('css/plugins/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!--Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom CSS -->
    <link href="{{ asset('css/custom.css?v='.time()) }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
  
    <!-- include summernote css/js-->
    <link href="{{ asset('css/summernote.css') }}">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
    
    @yield('css')

</head>

<section class="LoginSection">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="LogoImg">
                    <a href="{{url('/')}}" class="navbar-brand"><img src="{{asset('images/LoginLogo.png')}}"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 ">
                <div class="panel panel-default">
                    <!-- <div class="panel-heading">Login</div> -->
                        <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <h1>WELCOME BACK!</h1>
                               <!--  <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->
                               <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <input id="email" type="email" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                   <!--  <label for="password" class="col-md-4 control-label">Password</label> -->
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                        Forgot Your Password?
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="ctr">
                                        <button type="submit" class="btn btn-primary">Login Now</button>

                                    </div>
                                    <button style ="visibility:hidden" type="submit" >
                                        Login
                                    </button>
                                    
                                </div>
                                
                            </div>
                            <div class="row">
                                 <div class="ctr top-line">
                                     <h4> Register As </h4>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                    <div class="ctr">
                                        <a class="btn btn-outline" href="{{ route('clientregister') }}">
                                     Client
                                        </a>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="ctr">
                                          <a class="btn btn btn-outline" href="{{ route('coachregister') }}">
                                            Coach
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="ctr MrTp">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis sagittis augue. In ut accumsan nisl. Quisque velit dolor, ullamcorper sed ex id, sollicitudin maximus tortor. In non luctus ipsum, et posuere dui.</p>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</section>
<footer class="custom-footer">
    <div class="container">
        <div class="row top-line2">
            <div class="col-md-12">
                <div class="ctr">
                    <p>Copyright ©  2020 Business Bullseye. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>  
</footer>

