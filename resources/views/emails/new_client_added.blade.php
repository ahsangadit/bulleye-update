<p>Hi {{$user->name}},</p>
    
<p>Welcome to Business Bullseye. You are now registered for <a href="{{url("/home")}}">{{$package->title}}</a></p>
    
<p>To get started, log-in to <a href="{{url("/login")}}">http://www.business-bullseye.com/login</a></p>

<p>Your Credentials Below: </p>

<p><b> Email     : {{$user->email}} </b></p>
<p><b> Password  : {{$password}} </b></p>
    
<p>If you need assistance, please contact us either<br/>
by phone: 805-451-4390<br/>
by email: <a href="mailto:julie@straightforwardsuccess.com">julie@straightforwardsuccess.com</a></p>

<p>To your continued success!<br/>
Anthony McGloin</p> 

<!--<p>You have been added to the Package <a href="{{url("/home")}}">{{$package->title}}</a>.</p>-->
Regards<br/>
Admin