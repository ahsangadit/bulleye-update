@extends('layouts.app')

@section('heading')
&nbsp;<!-- col-md-11 Admin User - <small>Add</small>-->
@endsection

@section('content')
<section class="LoginSection">
    <div class="container">
        @php
        $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
      @endphp

        <div class="row MrTp80 ">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                   <!--  <div class="panel-heading">Register As Client</div> -->
                    <div class="panel-body paypal">
                        @if(Session::has('responseerror'))
                      
                            <div class="row"> 
                                <div class="col-md-12"> 
                                    <div class="custom-alerts alert alert-danger"><em> {!! session('responseerror') !!}</em> 
                                   </div> 
                               </div> 
                           </div> 
                       
                       @endif 
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('coachstore') }}">
                            {{ csrf_field() }}
                              <!--   <label for="name" class="col-md-4 control-label">Name</label> -->
                            <div class="row">
                                <h1>Register As Coach</h1>
                                     <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                               <!--  <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                                <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                         </div>
                                </div>
                            </div>
                               
                            <div class="row">
                                 <div class="col-md-6">
                                     <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                     </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="top-line MrTp p-1 ">
                                    <div class="col-md-6">
                                        <div class="form-group ctr">
                                            <label >Payment Frequency</label>
                                          </div>
                                    </div>
                                    <div class="col-md-6">
                                       <ul class="choice">
                                           <li> <input type="radio" id="monthly"  name="payment_frequency" value="monthly" checked>
                                            <label for="monthly">Monthly ($5) </label></li>
                                           <li>  <input type="radio" id="yearly" name="payment_frequency" value="yearly">
                                            <label for="yearly">Yearly ($ 50)</label></li>
                                       </ul> 
                                    </div>
                                </div>
                            </div>
                                

                           <div class="row p-1 ">
                                <div class="col-md-2"></div>
                                 <div class="col-md-4">
                                     <div class="form-group">
                                        <button type="submit" class="btn btn-outline">
                                            Register (Paypal)
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                     <div class="form-group">
                                    <a href="#_" id="authorizenet" class="btn btn-primary">
                                        Authorize.net
                                    </a>
                                    </div>
                                </div>
                                 <div class="col-md-2"></div>
                                
                           </div>
                        </form>
                    </div>

                    <div class="panel-body authorize">
                        <h2 style="text-align:center"><h1>Register Through Authorize.net</h1><br>
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('coachregisterthroughauthorize') }}">
                            {{ csrf_field() }}

                            <div class="row">
                                    <div class="col-md-6">
                                         <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="cardnumber" type="text" class="form-control" name="cardnumber" placeholder="Card Number" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                     
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="cvv" name="cvv" value="" placeholder="Please enter cvv" required>
                                        </div>
                                
                                     
                                </div>
                              
                            </div>                   
                                   <!--  <label for="cardnumber" class="col-md-4 control-label"></label> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="cvv">Expire Date</label>
                                </div>
                            </div>
                            <div class="row">
                                      <div class="col-md-6">
                                              <div class="form-group">
                                                            <select class="form-control" id="expiration-month" name="expiration_month" >
                                                            @foreach($months as $k=>$v)
                                                                <option value="{{ $k }}">{{ $v }}</option>                                                        
                                                            @endforeach
                                                            </select> 
                                            </div>       
                                    </div>
                                    <div class="col-md-6">
                                         <div class="form-group">
                                            <select class="form-control" id="expiration-year" name="expiration_year" >
                                                                    
                                                @for($i = date('Y'); $i <= (date('Y') + 15); $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>            
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    
                            </div>
                            <div class="row top-line MrTp p-1 ">
                                <div class="col-md-6">
                                     <label >Payment Frequency</label>
                                   
                                </div>
                                <div class="col-md-6">
                                    <ul class="choice">
                                        <li> 
                                            <input type="radio" id="monthly"  name="payment_frequency" value="monthly" checked>
                                            <label for="monthly">Monthly ($10) </label>
                                        </li>
                                        <li>
                                        <input type="radio" id="yearly" name="payment_frequency" value="yearly">
                                            <label for="yearly">yearly ($100)</label>
                                        </li>
                                    </ul>
                                    
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline">
                                        Register (Authorize.net)
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <a href="#_" id="paypal" class="btn btn-primary FullWidth">
                                            Paypal
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
             <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="ctr MrTp">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis sagittis augue. In ut accumsan nisl. Quisque velit dolor, ullamcorper sed ex id, sollicitudin maximus tortor. In non luctus ipsum, et posuere dui.</p>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row top-line2">
            <div class="col-md-12">
                <div class="ctr">
                    <p>Copyright ©  2020 Business Bullseye. All rights reserved.</p>
                </div>
            </div>
        </div>
            
    </div>   
</section>
 
@endsection
