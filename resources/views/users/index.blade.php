

@extends('layouts.app')

@section('content')

<script type="text/javascript">
    function confirm_delete() {
      return confirm('are you sure?');
    }
    </script>
<div class="">
    <div class="row">
        <div class="col-md-12 ">
            <!--<button id="btn_add_package" name="btn_add_package" class="btn btn-secondary pull-right" >New Package</button>-->
            @if ($message = Session::get('success'))
            <div class="success-notification" message="{{ $message }}">
            </div>

            @endif

            <!--Clients Order by Packages-->
            <div class="panel-body"> 
<?php $num =1; ?>

                <table class="table">
                    <thead>
                        <tr>
                            <!--<th style="width: 14px;"></th>-->
                            <th>S.no </th>
                            <th>Name </th>
                            <th>Email </th>
                            <th>Action </th>
                            <!--<th> </th>
                            <th> </th>-->
                        </tr>
                    </thead>
                    <tbody id="clients-list" name="clients-list">
                        @foreach ($users as $users)
                        <tr>
                            <td>{{$num}}</td>
                            <td>{{$users->name}} </td>
                            <td>{{$users->email}} </td>
                            <td style="display:flex;">
                                <a href="{{route('editusers', $users->id)}}" class="btn btn-secondary" title="Edit"><i class="fa fa-edit"></i></a>
                                <form action="{{ route('deleteusers', $users->id) }}" method="POST" >
                                    @csrf
                                    @method('DELETE')
                                <button onclick="return confirm_delete()" type="submit" class="btn btn-danger " value="{{$users->id}} " id="" title="Delete">
                                    <i class="fa fa-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                        <?php $num++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!--       incluides here-->
    </div>



    

</div>

@endsection

@section('heading')
Admins <!--<small>management</small>-->
@endsection

@section('title')
Clients
@endsection

@section('script')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--<script src="{{asset('js/client.js')}}"></script>-->
@endsection
